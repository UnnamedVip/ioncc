import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private card: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.card = this.formBuilder.group({
      cardNumber: ['', Validators.compose([Validators.required, Validators.maxLength(19), Validators.minLength(19)])],
      cardOwner: ['', Validators.required]
    });

    this.card.controls['cardNumber'].valueChanges.subscribe(data => {
      this.checkcardNumber(data);
    });
  }

  //event SEND button
  logForm() {
    console.log(this.card.value)
  }

  checkcardNumber(data: string) {

    if (data.length < 19) {
      let tmpData = data.replace(/[^0-9]/gi, '')
      if (tmpData.length % 4 == 0) {
        this.card.patchValue({ cardNumber: data + '-' }, { emitEvent: false });
      }
    } else {
      this.card.patchValue({ cardNumber: data.substr(0, 19) }, { emitEvent: false });
    }
  }
}
